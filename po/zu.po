# Zulu translation for tp-account-widgets
# This file is distributed under the same license as the tp-account-widgets package.
# Abel Motha, Translate.org.za <sedric@translate.org.za>, 2010, 2011.
# Priscilla Mahlangu <priny@translate.org.za>, 2011.
# F Wolff <friedel@translate.org.za>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: empathy\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-07-31 15:46+0100\n"
"PO-Revision-Date: 2011-07-29 22:35+0200\n"
"Last-Translator: Priscilla Mahlangu <priny@translate.org.za>\n"
"Language-Team: translate-discuss-af@lists.sourceforge.net\n"
"Language: zu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.0\n"
"X-Project-Style: gnome\n"

#: ../data/org.gnome.telepathy-account-widgets.gschema.xml.h:1
msgid "Default directory to select an avatar image from"
msgstr "Umkhombandlela wezimiso ongakhetha kuwo isithombe esiyisifaniso muntu"

#: ../data/org.gnome.telepathy-account-widgets.gschema.xml.h:2
msgid "The last directory that an avatar image was chosen from."
msgstr ""
"Umkhombandlela wokugcina lapho kukhethwe khona isithombe isingumfaniso muntu."

#: ../tp-account-widgets/totem-subtitle-encoding.c:157
msgid "Current Locale"
msgstr "Indawo yamanje"

#: ../tp-account-widgets/totem-subtitle-encoding.c:160
#: ../tp-account-widgets/totem-subtitle-encoding.c:162
#: ../tp-account-widgets/totem-subtitle-encoding.c:164
#: ../tp-account-widgets/totem-subtitle-encoding.c:166
msgid "Arabic"
msgstr "isi-Arabhu"

#: ../tp-account-widgets/totem-subtitle-encoding.c:169
msgid "Armenian"
msgstr "isi-Ameniyani"

#: ../tp-account-widgets/totem-subtitle-encoding.c:172
#: ../tp-account-widgets/totem-subtitle-encoding.c:174
#: ../tp-account-widgets/totem-subtitle-encoding.c:176
msgid "Baltic"
msgstr "isi-Bhalthikhi"

#: ../tp-account-widgets/totem-subtitle-encoding.c:179
msgid "Celtic"
msgstr "isi-Selthikhi"

#: ../tp-account-widgets/totem-subtitle-encoding.c:182
#: ../tp-account-widgets/totem-subtitle-encoding.c:184
#: ../tp-account-widgets/totem-subtitle-encoding.c:186
#: ../tp-account-widgets/totem-subtitle-encoding.c:188
msgid "Central European"
msgstr "Maphakathi neYurophu"

#: ../tp-account-widgets/totem-subtitle-encoding.c:191
#: ../tp-account-widgets/totem-subtitle-encoding.c:193
#: ../tp-account-widgets/totem-subtitle-encoding.c:195
#: ../tp-account-widgets/totem-subtitle-encoding.c:197
msgid "Chinese Simplified"
msgstr "isi-Shayina esenziwe lula"

#: ../tp-account-widgets/totem-subtitle-encoding.c:200
#: ../tp-account-widgets/totem-subtitle-encoding.c:202
#: ../tp-account-widgets/totem-subtitle-encoding.c:204
msgid "Chinese Traditional"
msgstr "isi-Shayina sendabuko"

#: ../tp-account-widgets/totem-subtitle-encoding.c:207
msgid "Croatian"
msgstr "isi-Khroweshiyeni"

#: ../tp-account-widgets/totem-subtitle-encoding.c:210
#: ../tp-account-widgets/totem-subtitle-encoding.c:212
#: ../tp-account-widgets/totem-subtitle-encoding.c:214
#: ../tp-account-widgets/totem-subtitle-encoding.c:216
#: ../tp-account-widgets/totem-subtitle-encoding.c:218
#: ../tp-account-widgets/totem-subtitle-encoding.c:220
msgid "Cyrillic"
msgstr "isi-Sayrillikhi"

#: ../tp-account-widgets/totem-subtitle-encoding.c:223
msgid "Cyrillic/Russian"
msgstr "isi-Sayrillikhi/Rashiyeni"

#: ../tp-account-widgets/totem-subtitle-encoding.c:226
#: ../tp-account-widgets/totem-subtitle-encoding.c:228
msgid "Cyrillic/Ukrainian"
msgstr "isi-Sayrillikhi/Yukhreniyeni"

#: ../tp-account-widgets/totem-subtitle-encoding.c:231
msgid "Georgian"
msgstr "isi-Jiyojiyeni"

#: ../tp-account-widgets/totem-subtitle-encoding.c:234
#: ../tp-account-widgets/totem-subtitle-encoding.c:236
#: ../tp-account-widgets/totem-subtitle-encoding.c:238
msgid "Greek"
msgstr "isi-Grigi"

#: ../tp-account-widgets/totem-subtitle-encoding.c:241
msgid "Gujarati"
msgstr "isi-Gujarati"

#: ../tp-account-widgets/totem-subtitle-encoding.c:244
msgid "Gurmukhi"
msgstr "isi-Gumukhi"

#: ../tp-account-widgets/totem-subtitle-encoding.c:247
#: ../tp-account-widgets/totem-subtitle-encoding.c:249
#: ../tp-account-widgets/totem-subtitle-encoding.c:251
#: ../tp-account-widgets/totem-subtitle-encoding.c:253
msgid "Hebrew"
msgstr "isi-Hebheru"

#: ../tp-account-widgets/totem-subtitle-encoding.c:256
msgid "Hebrew Visual"
msgstr "isi-Hebheru (visual)"

#: ../tp-account-widgets/totem-subtitle-encoding.c:259
msgid "Hindi"
msgstr "isi-Hindi"

#: ../tp-account-widgets/totem-subtitle-encoding.c:262
msgid "Icelandic"
msgstr "isi-Icelandic"

#: ../tp-account-widgets/totem-subtitle-encoding.c:265
#: ../tp-account-widgets/totem-subtitle-encoding.c:267
#: ../tp-account-widgets/totem-subtitle-encoding.c:269
msgid "Japanese"
msgstr "isi-Japani"

#: ../tp-account-widgets/totem-subtitle-encoding.c:272
#: ../tp-account-widgets/totem-subtitle-encoding.c:274
#: ../tp-account-widgets/totem-subtitle-encoding.c:276
#: ../tp-account-widgets/totem-subtitle-encoding.c:278
msgid "Korean"
msgstr "isi-Khoriyeni"

#: ../tp-account-widgets/totem-subtitle-encoding.c:281
msgid "Nordic"
msgstr "isi-Nodikhi"

#: ../tp-account-widgets/totem-subtitle-encoding.c:284
msgid "Persian"
msgstr "isi-Pheshiyeni"

#: ../tp-account-widgets/totem-subtitle-encoding.c:287
#: ../tp-account-widgets/totem-subtitle-encoding.c:289
msgid "Romanian"
msgstr "isi-Romeni"

#: ../tp-account-widgets/totem-subtitle-encoding.c:292
msgid "South European"
msgstr "isi-Niningizimu Yurophu"

#: ../tp-account-widgets/totem-subtitle-encoding.c:295
msgid "Thai"
msgstr "isi-Thayi"

#: ../tp-account-widgets/totem-subtitle-encoding.c:298
#: ../tp-account-widgets/totem-subtitle-encoding.c:300
#: ../tp-account-widgets/totem-subtitle-encoding.c:302
#: ../tp-account-widgets/totem-subtitle-encoding.c:304
msgid "Turkish"
msgstr "isi-Thekishi"

#: ../tp-account-widgets/totem-subtitle-encoding.c:307
#: ../tp-account-widgets/totem-subtitle-encoding.c:309
#: ../tp-account-widgets/totem-subtitle-encoding.c:311
#: ../tp-account-widgets/totem-subtitle-encoding.c:313
#: ../tp-account-widgets/totem-subtitle-encoding.c:315
msgid "Unicode"
msgstr "i-Yunikhodi"

#: ../tp-account-widgets/totem-subtitle-encoding.c:318
#: ../tp-account-widgets/totem-subtitle-encoding.c:320
#: ../tp-account-widgets/totem-subtitle-encoding.c:322
#: ../tp-account-widgets/totem-subtitle-encoding.c:324
#: ../tp-account-widgets/totem-subtitle-encoding.c:326
msgid "Western"
msgstr "Intshonalanga"

#: ../tp-account-widgets/totem-subtitle-encoding.c:329
#: ../tp-account-widgets/totem-subtitle-encoding.c:331
#: ../tp-account-widgets/totem-subtitle-encoding.c:333
msgid "Vietnamese"
msgstr "isi-Vietnamese"

#: ../tp-account-widgets/tpaw-account-widget-aim.ui.h:1
#: ../tp-account-widgets/tpaw-account-widget-groupwise.ui.h:1
#: ../tp-account-widgets/tpaw-account-widget-icq.ui.h:1
#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:14
#: ../tp-account-widgets/tpaw-account-widget-msn.ui.h:1
#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:1
#, fuzzy
msgid "Pass_word"
msgstr "Igama_lokungena:"

#: ../tp-account-widgets/tpaw-account-widget-aim.ui.h:2
#, fuzzy
msgid "Screen _Name"
msgstr "Igama _lesihenqo:"

#: ../tp-account-widgets/tpaw-account-widget-aim.ui.h:3
msgid "<b>Example:</b> MyScreenName"
msgstr "<b>Isibonelo:</b> MyScreenName"

#: ../tp-account-widgets/tpaw-account-widget-aim.ui.h:4
#: ../tp-account-widgets/tpaw-account-widget-groupwise.ui.h:4
#: ../tp-account-widgets/tpaw-account-widget-icq.ui.h:4
#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:12
#: ../tp-account-widgets/tpaw-account-widget-msn.ui.h:4
#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:4
#: ../tp-account-widgets/tpaw-account-widget-yahoo.ui.h:4
msgid "Remember password"
msgstr "Khumbula igama lokungena"

#: ../tp-account-widgets/tpaw-account-widget-aim.ui.h:5
#: ../tp-account-widgets/tpaw-account-widget-groupwise.ui.h:5
#: ../tp-account-widgets/tpaw-account-widget-icq.ui.h:6
#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:21
#: ../tp-account-widgets/tpaw-account-widget-msn.ui.h:5
#, fuzzy
msgid "_Port"
msgstr "_Imbobo yokuxhumana:"

#: ../tp-account-widgets/tpaw-account-widget-aim.ui.h:6
#: ../tp-account-widgets/tpaw-account-widget-groupwise.ui.h:6
#: ../tp-account-widgets/tpaw-account-widget-icq.ui.h:7
#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:20
#: ../tp-account-widgets/tpaw-account-widget-msn.ui.h:6
#, fuzzy
msgid "_Server"
msgstr "_Isiphakeli:"

#: ../tp-account-widgets/tpaw-account-widget-aim.ui.h:7
#: ../tp-account-widgets/tpaw-account-widget-generic.ui.h:1
#: ../tp-account-widgets/tpaw-account-widget-groupwise.ui.h:7
#: ../tp-account-widgets/tpaw-account-widget-icq.ui.h:8
#: ../tp-account-widgets/tpaw-account-widget-irc.ui.h:16
#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:23
#: ../tp-account-widgets/tpaw-account-widget-local-xmpp.ui.h:7
#: ../tp-account-widgets/tpaw-account-widget-msn.ui.h:7
#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:22
#: ../tp-account-widgets/tpaw-account-widget-yahoo.ui.h:9
msgid "Advanced"
msgstr "Okuseqophelweni"

#: ../tp-account-widgets/tpaw-account-widget-aim.ui.h:8
msgid "What is your AIM screen name?"
msgstr "Yini igama lesihenqo sakho se-AIM?"

#: ../tp-account-widgets/tpaw-account-widget-aim.ui.h:9
msgid "What is your AIM password?"
msgstr "Yini igama lakho lokungena le-AIM?"

#: ../tp-account-widgets/tpaw-account-widget-aim.ui.h:10
#: ../tp-account-widgets/tpaw-account-widget-groupwise.ui.h:10
#: ../tp-account-widgets/tpaw-account-widget-icq.ui.h:11
#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:7
#: ../tp-account-widgets/tpaw-account-widget-msn.ui.h:10
#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:25
msgid "Remember Password"
msgstr "Khumbula igama lokungena"

#: ../tp-account-widgets/tpaw-account-widget-groupwise.ui.h:2
#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:13
#: ../tp-account-widgets/tpaw-account-widget-msn.ui.h:2
#, fuzzy
msgid "Login I_D"
msgstr "Ngena I_D:"

#: ../tp-account-widgets/tpaw-account-widget-groupwise.ui.h:3
#: ../tp-account-widgets/tpaw-account-widget-yahoo.ui.h:3
msgid "<b>Example:</b> username"
msgstr "<b>Isibonelo:</b> igama lomsebenzisi"

#: ../tp-account-widgets/tpaw-account-widget-groupwise.ui.h:8
msgid "What is your GroupWise User ID?"
msgstr "Yini i-ID yakho yomsebenzisi ye-GroupWise?"

#: ../tp-account-widgets/tpaw-account-widget-groupwise.ui.h:9
msgid "What is your GroupWise password?"
msgstr "Yini igama lakho lokungena le-GroupWise?"

#: ../tp-account-widgets/tpaw-account-widget-icq.ui.h:2
#, fuzzy
msgid "ICQ _UIN"
msgstr "ICQ _UIN:"

#: ../tp-account-widgets/tpaw-account-widget-icq.ui.h:3
msgid "<b>Example:</b> 123456789"
msgstr "<b>Isibonelo:</b> 123456789"

#: ../tp-account-widgets/tpaw-account-widget-icq.ui.h:5
#, fuzzy
msgid "Ch_aracter set"
msgstr "Uh_lamvusimo lumisime:"

#: ../tp-account-widgets/tpaw-account-widget-icq.ui.h:9
msgid "What is your ICQ UIN?"
msgstr "Yini i-ICQ UIN yakho?"

#: ../tp-account-widgets/tpaw-account-widget-icq.ui.h:10
msgid "What is your ICQ password?"
msgstr "Yini igama lakho lokungena le ICQ?"

#: ../tp-account-widgets/tpaw-account-widget-irc.ui.h:1
msgid "Network"
msgstr "Inethiwekhi"

#: ../tp-account-widgets/tpaw-account-widget-irc.ui.h:2
#, fuzzy
msgid "Character set"
msgstr "Uhlamvusimo lumisiwe:"

#: ../tp-account-widgets/tpaw-account-widget-irc.ui.h:3
msgid "Add…"
msgstr "Engeza…"

#: ../tp-account-widgets/tpaw-account-widget-irc.ui.h:4
msgid "Remove"
msgstr "Khipha"

#. Translators: tooltip on a 'Go Up' button used to sort IRC servers by priority
#: ../tp-account-widgets/tpaw-account-widget-irc.ui.h:6
msgid "Up"
msgstr ""

#. Translators: tooltip on a 'Go Down' button used to sort IRC servers by priority
#: ../tp-account-widgets/tpaw-account-widget-irc.ui.h:8
msgid "Down"
msgstr ""

#: ../tp-account-widgets/tpaw-account-widget-irc.ui.h:9
msgid "Servers"
msgstr "Iziphakeli"

#: ../tp-account-widgets/tpaw-account-widget-irc.ui.h:10
msgid ""
"Most IRC servers don't need a password, so if you're not sure, don't enter a "
"password."
msgstr ""

#: ../tp-account-widgets/tpaw-account-widget-irc.ui.h:11
#, fuzzy
msgid "Nickname"
msgstr "Isiteketiso:"

#: ../tp-account-widgets/tpaw-account-widget-irc.ui.h:12
msgid "Password"
msgstr "Igama lokungena"

#: ../tp-account-widgets/tpaw-account-widget-irc.ui.h:13
#, fuzzy
msgid "Quit message"
msgstr "Umlayezo wokuphuma:"

#: ../tp-account-widgets/tpaw-account-widget-irc.ui.h:14
#, fuzzy
msgid "Real name"
msgstr "Igama langempela:"

#: ../tp-account-widgets/tpaw-account-widget-irc.ui.h:15
#, fuzzy
msgid "Username"
msgstr "Igama lomsebenzisi:"

#: ../tp-account-widgets/tpaw-account-widget-irc.ui.h:17
msgid "Which IRC network?"
msgstr "Iyiphi inethiwekhi ye-IRC?"

#: ../tp-account-widgets/tpaw-account-widget-irc.ui.h:18
msgid "What is your IRC nickname?"
msgstr "Yini isiteketiso sakho seIRC?"

#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:1
msgid "What is your Facebook username?"
msgstr "Yini igama lakho lomsebenzisi le Facebook?"

#. This string is not wrapped in the dialog so you may have to add some '\n' to make it look nice.
#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:3
msgid ""
"This is your username, not your normal Facebook login.\n"
"If you are facebook.com/<b>badger</b>, enter <b>badger</b>.\n"
"Use <a href=\"http://www.facebook.com/username/\">this page</a> to choose a "
"Facebook username if you don't have one."
msgstr ""
"Leli igama lakho lomsebenzisi, hhayi igama lokungena elijwayelekile lakho le-"
"Facebook.\n"
"Uma ngabe ungu-facebook.com/<b>i-badger</b>, faka <b>i-badger</b>.\n"
"Sebenzisa <a href=\"http://www.facebook.com/username/\">leli khasi</a> "
"ukukhetha igama lomsebenzisi le Facebook uma ngabe ungenalo."

#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:6
msgid "What is your Facebook password?"
msgstr "Yini igama lakho lokungena le Facebook?"

#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:8
msgid "What is your Google ID?"
msgstr "Yini i-ID yakho ye-Google?"

#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:9
msgid "<b>Example:</b> user@gmail.com"
msgstr "<b>Isibonelo:</b> user@gmail.com"

#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:10
msgid "What is your Google password?"
msgstr "Yini igama lakho lokungena le-Google?"

#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:11
msgid "<b>Example:</b> user@jabber.org"
msgstr "<b>Isibonelo:</b> user@jabber.org"

#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:15
msgid "I_gnore SSL certificate errors"
msgstr "Shaya i_ndiva amaphutha wesitifiketi se SSL"

#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:16
#, fuzzy
msgid "Priori_ty"
msgstr "Okusem_qoka:"

#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:17
#, fuzzy
msgid "Reso_urce"
msgstr "Insi_za:"

#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:18
msgid "Encr_yption required (TLS/SSL)"
msgstr "Ukub_hala okuyimfihlo kuyadingeka (TLS/SSL)"

#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:19
msgid "Override server settings"
msgstr "Dlova izimiso zesiphakeli"

#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:22
msgid "Use old SS_L"
msgstr "Sebenzisa endala i-SS_L"

#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:24
msgid "What is your Jabber ID?"
msgstr "Yini i-ID yakho ye-Jabber?"

#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:25
msgid "What is your desired Jabber ID?"
msgstr "Yini i-ID yakho ye-Jabber oyifisayo?"

#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:26
msgid "What is your Jabber password?"
msgstr "Yini igama lakho lokungena le-Jabber?"

#: ../tp-account-widgets/tpaw-account-widget-jabber.ui.h:27
msgid "What is your desired Jabber password?"
msgstr "Yini igama lokungena le-Jabber olifisayo?"

#: ../tp-account-widgets/tpaw-account-widget-local-xmpp.ui.h:1
#, fuzzy
msgid "Nic_kname"
msgstr "Isi_teketiso:"

#: ../tp-account-widgets/tpaw-account-widget-local-xmpp.ui.h:2
#, fuzzy
msgid "_Last Name"
msgstr "_Isibongo:"

#: ../tp-account-widgets/tpaw-account-widget-local-xmpp.ui.h:3
#, fuzzy
msgid "_First Name"
msgstr "_Igama lokuqala:"

#: ../tp-account-widgets/tpaw-account-widget-local-xmpp.ui.h:4
#, fuzzy
msgid "_Published Name"
msgstr "Igama _elishicilelwe:"

#: ../tp-account-widgets/tpaw-account-widget-local-xmpp.ui.h:5
#, fuzzy
msgid "_Jabber ID"
msgstr "I-ID ye-_Jabber:"

#: ../tp-account-widgets/tpaw-account-widget-local-xmpp.ui.h:6
#, fuzzy
msgid "E-_mail address"
msgstr "I_kheli le-imeyli:"

#: ../tp-account-widgets/tpaw-account-widget-msn.ui.h:3
msgid "<b>Example:</b> user@hotmail.com"
msgstr "<b>Isibonelo</b> user@hotmail.com"

#: ../tp-account-widgets/tpaw-account-widget-msn.ui.h:8
msgid "What is your Windows Live ID?"
msgstr "Yini i-ID yakho ye-Windows Live?"

#: ../tp-account-widgets/tpaw-account-widget-msn.ui.h:9
msgid "What is your Windows Live password?"
msgstr "Yini igama lokungena lakho le-Windows Live?"

#: ../tp-account-widgets/tpaw-account-widget-sip.c:198
#: ../tp-account-widgets/tpaw-account-widget-sip.c:231
msgid "Auto"
msgstr "Ukuzenzakalela"

#: ../tp-account-widgets/tpaw-account-widget-sip.c:201
msgid "UDP"
msgstr "UDP"

#: ../tp-account-widgets/tpaw-account-widget-sip.c:204
msgid "TCP"
msgstr "TCP"

#: ../tp-account-widgets/tpaw-account-widget-sip.c:207
msgid "TLS"
msgstr "TLS"

#: ../tp-account-widgets/tpaw-account-widget-sip.c:236
msgid "Register"
msgstr "Bhalisa"

#: ../tp-account-widgets/tpaw-account-widget-sip.c:241
msgid "Options"
msgstr "Izinketho"

#: ../tp-account-widgets/tpaw-account-widget-sip.c:244
msgid "None"
msgstr "Lutho"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:2
#, fuzzy
msgid "_Username"
msgstr "_Igama lomsebenzisi:"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:3
msgid "<b>Example:</b> user@my.sip.server"
msgstr "<b>Isibonelo:</b> user@my.sip.server"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:5
msgid "Use this account to call _landlines and mobile phones"
msgstr ""

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:6
msgid "NAT Traversal Options"
msgstr "Izinkethi ze-NAT Traversal"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:7
msgid "Proxy Options"
msgstr "Izinkethi zomnikezeli"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:8
msgid "Miscellaneous Options"
msgstr "Izinketho eziyingxubevange"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:9
#, fuzzy
msgid "STUN Server"
msgstr "Isiphakeli ye-STUN:"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:10
msgid "Discover the STUN server automatically"
msgstr "Thola isiphakeli se-STUN ngokuzenzakalela"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:11
msgid "Discover Binding"
msgstr "Thola ukubophezela"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:12
#: ../tp-account-widgets/tpaw-irc-network-dialog.c:500
msgid "Server"
msgstr "Isiphakeli"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:13
msgid "Keep-Alive Options"
msgstr "Gcina izinkethi ziphila"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:14
#, fuzzy
msgid "Mechanism"
msgstr "Umshini:"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:15
msgid "Interval (seconds)"
msgstr "Isikhawu (imizuzwana)"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:16
#, fuzzy
msgid "Authentication username"
msgstr "Kuqinisekiswa igama lomsebenzisi:"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:17
#, fuzzy
msgid "Transport"
msgstr "Thutha:"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:18
msgid "Loose Routing"
msgstr "Ukuthutha okuxegayo"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:19
msgid "Ignore TLS Errors"
msgstr "Unganaki amaphutha we-TLS"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:20
#: ../tp-account-widgets/tpaw-irc-network-dialog.c:521
msgid "Port"
msgstr "Imbobo yokuxhumana"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:21
msgid "Local IP Address"
msgstr ""

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:23
msgid "What is your SIP login ID?"
msgstr "Yini i-ID ykho yokungena ye-SIP?"

#: ../tp-account-widgets/tpaw-account-widget-sip.ui.h:24
msgid "What is your SIP account password?"
msgstr "Yini igama lakho lokungena kwi-akhawunti ye-SIP?"

#: ../tp-account-widgets/tpaw-account-widget-yahoo.ui.h:1
msgid "Pass_word:"
msgstr "Igama_lokungena:"

#: ../tp-account-widgets/tpaw-account-widget-yahoo.ui.h:2
msgid "Yahoo! I_D:"
msgstr "I-Yahoo! I_D:"

#: ../tp-account-widgets/tpaw-account-widget-yahoo.ui.h:5
msgid "I_gnore conference and chat room invitations"
msgstr "Shaya i_ndiva izimemo zenkomfa kanye nezegumbi lokuxoxa"

#: ../tp-account-widgets/tpaw-account-widget-yahoo.ui.h:6
msgid "_Room List locale:"
msgstr "_Uhlu lwegumbi lendawo:"

#: ../tp-account-widgets/tpaw-account-widget-yahoo.ui.h:7
msgid "Ch_aracter set:"
msgstr "Uh_lamvusimo lumisime:"

#: ../tp-account-widgets/tpaw-account-widget-yahoo.ui.h:8
msgid "_Port:"
msgstr "_Imbobo yokuxhumana:"

#: ../tp-account-widgets/tpaw-account-widget-yahoo.ui.h:10
msgid "What is your Yahoo! ID?"
msgstr "Yini i-Yahoo! ID yakho?"

#: ../tp-account-widgets/tpaw-account-widget-yahoo.ui.h:11
msgid "What is your Yahoo! password?"
msgstr "Yini igama lokungena lakho le Yahoo?"

#: ../tp-account-widgets/tpaw-calendar-button.c:63
#, fuzzy
msgid "Select..."
msgstr "Khetha"

#: ../tp-account-widgets/tpaw-calendar-button.c:151
#, fuzzy
msgid "_Select"
msgstr "Khetha"

#: ../tp-account-widgets/tpaw-contactinfo-utils.c:96
#, fuzzy
msgid "Full name"
msgstr "Igama eliphelele:"

#: ../tp-account-widgets/tpaw-contactinfo-utils.c:97
#, fuzzy
msgid "Phone number"
msgstr "Inombolo yocingo:"

#: ../tp-account-widgets/tpaw-contactinfo-utils.c:98
#, fuzzy
msgid "E-mail address"
msgstr "Ikheli le-imeyli:"

#: ../tp-account-widgets/tpaw-contactinfo-utils.c:99
#, fuzzy
msgid "Website"
msgstr "I-webhusaythi:"

#: ../tp-account-widgets/tpaw-contactinfo-utils.c:100
#, fuzzy
msgid "Birthday"
msgstr "Usuku lokuzalwa:"

#. Note to translators: this is the caption for a string of the form "5
#. * minutes ago", and refers to the time since the contact last interacted
#. * with their IM client.
#: ../tp-account-widgets/tpaw-contactinfo-utils.c:105
msgid "Last seen:"
msgstr "Ugcine ukubonwa:"

#: ../tp-account-widgets/tpaw-contactinfo-utils.c:106
msgid "Server:"
msgstr "Isiphakeli:"

#: ../tp-account-widgets/tpaw-contactinfo-utils.c:107
msgid "Connected from:"
msgstr "Ixhunywe kusuka ku:"

#. FIXME: once Idle implements SimplePresence using this information, we can
#. * and should bin this.
#: ../tp-account-widgets/tpaw-contactinfo-utils.c:111
msgid "Away message:"
msgstr "Umlayezo wokuba kude:"

#: ../tp-account-widgets/tpaw-contactinfo-utils.c:124
#, fuzzy
msgid "work"
msgstr "Inethiwekhi"

#: ../tp-account-widgets/tpaw-contactinfo-utils.c:125
msgid "home"
msgstr ""

#: ../tp-account-widgets/tpaw-contactinfo-utils.c:126
msgid "mobile"
msgstr ""

#: ../tp-account-widgets/tpaw-contactinfo-utils.c:127
msgid "voice"
msgstr ""

#: ../tp-account-widgets/tpaw-contactinfo-utils.c:128
#, fuzzy
msgid "preferred"
msgstr "Okuthandwayo"

#: ../tp-account-widgets/tpaw-contactinfo-utils.c:129
msgid "postal"
msgstr ""

#: ../tp-account-widgets/tpaw-contactinfo-utils.c:130
msgid "parcel"
msgstr ""

#: ../tp-account-widgets/tpaw-irc-network-chooser-dialog.c:325
msgid "New Network"
msgstr "Inethiwekhi entsha"

#: ../tp-account-widgets/tpaw-irc-network-chooser-dialog.c:526
msgid "Choose an IRC network"
msgstr "Khetha inethiwekhi ye IRC"

#: ../tp-account-widgets/tpaw-irc-network-chooser-dialog.c:612
msgid "Reset _Networks List"
msgstr "Setha kabusha uhlu lwama_nethiwekhi"

#: ../tp-account-widgets/tpaw-irc-network-chooser-dialog.c:616
msgctxt "verb displayed on a button to select an IRC network"
msgid "Select"
msgstr "Khetha"

#: ../tp-account-widgets/tpaw-irc-network-dialog.c:273
msgid "new server"
msgstr "isiphakeli esisha"

#: ../tp-account-widgets/tpaw-irc-network-dialog.c:536
msgid "SSL"
msgstr "I-SSL"

#: ../tp-account-widgets/tpaw-keyring.c:77
#: ../tp-account-widgets/tpaw-keyring.c:186
#, fuzzy
msgid "Password not found"
msgstr "Ibinzana alitholakali"

#: ../tp-account-widgets/tpaw-keyring.c:586
#, c-format
msgid "IM account password for %s (%s)"
msgstr ""

#: ../tp-account-widgets/tpaw-keyring.c:623
#, c-format
msgid "Password for chatroom '%s' on account %s (%s)"
msgstr ""

#. Create account
#. To translator: %s is the name of the protocol, such as "Google Talk" or
#. * "Yahoo!"
#.
#: ../tp-account-widgets/tpaw-protocol.c:62
#, c-format
msgid "New %s account"
msgstr "I-akhawunti %s entsha"

#: ../tp-account-widgets/tpaw-time.c:86
#, c-format
msgid "%d second ago"
msgid_plural "%d seconds ago"
msgstr[0] "umzuzwana ongu %d odlule"
msgstr[1] "imizuzwana engu %d edlule"

#: ../tp-account-widgets/tpaw-time.c:92
#, c-format
msgid "%d minute ago"
msgid_plural "%d minutes ago"
msgstr[0] "umzuzu ongu %d odlule"
msgstr[1] "imizuzu engu %d edlule"

#: ../tp-account-widgets/tpaw-time.c:98
#, c-format
msgid "%d hour ago"
msgid_plural "%d hours ago"
msgstr[0] "ihora elingu %d eledlule"
msgstr[1] "amahora angu %d adlule"

#: ../tp-account-widgets/tpaw-time.c:104
#, c-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "usuku eli- %d eledlule"
msgstr[1] "izinsuku ezingu %d ezedlule"

#: ../tp-account-widgets/tpaw-time.c:110
#, c-format
msgid "%d week ago"
msgid_plural "%d weeks ago"
msgstr[0] "evikini elingu- %d eledlule"
msgstr[1] "amaviki angu- %d adlule"

#: ../tp-account-widgets/tpaw-time.c:116
#, c-format
msgid "%d month ago"
msgid_plural "%d months ago"
msgstr[0] "inyanga engu- %d edlule"
msgstr[1] "izinyanga ezingu- %d ezedlule"

#: ../tp-account-widgets/tpaw-time.c:138
msgid "in the future"
msgstr "ngokuzayo"

#: ../tp-account-widgets/tpaw-user-info.c:394
msgid "Go online to edit your personal information."
msgstr ""

#. Setup id label
#: ../tp-account-widgets/tpaw-user-info.c:454
#, fuzzy
msgid "Identifier"
msgstr "Isihlonzi:"

#. Setup nickname entry
#: ../tp-account-widgets/tpaw-user-info.c:459
#, fuzzy
msgid "Alias"
msgstr "Isibizo:"

#: ../tp-account-widgets/tpaw-user-info.c:474
#, fuzzy
msgid "<b>Personal Details</b>"
msgstr "Imininingwane yezobuchwepheshe"
